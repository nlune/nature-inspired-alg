import numpy as np
import random
import itertools
import operator
import time

'''Knapsack Problem Hill Climbing
small neighbourhood: swap and transposition using intial random binary values
large neighbourhood: look at all combinations within constraint'''

w = [10, 300, 1, 200, 100]
v = [1000, 4000, 5000, 5000, 2000]
def random_start():
    '''assign boolean random to start state, with constraint max 400 sum of w'''
    W = 0 #total weight
    full = False
    x = [0, 0, 0, 0, 0] #decision which items to take
    while not full:
        for i in range(len(x)):
            r = random.randint(1,100) #gen random num
            if r <= 50:
                x[i] = 0 #don't take item
            elif r > 50:
                x[i] = 1 #take item
                W += w[i] #add weight to total
                if W >= 400: #if over limit
                    full = True
                    x[i] = 0 #remove that item
                    W -= w[i] #deduct the weight
    return x

def calc_value(array):
    ''' calculates total value of sack
    @param v: array of values
    @param array: binary choice value array'''
    V = 0
    for index, val in enumerate(array):
        if val == 1:
            V += v[index]

    return V

def calc_weight(x):
    ''' calculates total value of sack
    @param w: weight array
    @param x: binary choice value array'''
    W = 0
    for index, val in enumerate(x):
        if val == 1:
            W += w[index]

    return W


def fchc_swap_neighbour(x):
    '''swap with neighbour to right if diff values, and if improves overall value (V)'''
    old_v = calc_value(x) #calculate current value of sack
    for i in range(len(x)):
         if x[i%5] != x[(i+1) % 5]:
             r = x.copy() #copy sack
             r[i%5], r[(i+1) % 5] = r[(i+1) % 5], r[i%5] #switch the 2 diff neighbouring values
             v = calc_value(r)
             w = calc_weight(r)
             if  v > old_v and w <= 400: #if value is better than old one, and weight in limit
                 x = r #set x as tmp bag r
    return x

def hc_swap_neighbour(x):
    '''swap neighbour and pick best
        @param x: our array'''
    old_v = calc_value(x) #calculate current value of sack

    options = [] #store all viable options
    values = [] #zip later w their values

    for i in range(len(x)):
         if x[i%5] != x[(i+1) % 5]:
             r = x.copy() #copy sack
             r[i%5],r[(i+1) % 5] = r[(i+1) % 5], r[i%5] #switch the 2 diff neighbouring values
             v = calc_value(r)
             w = calc_weight(r)
             if  v > old_v and w <= 400: #if value is better than old one, and weight in limit
                 options.append(r) #add potential choices to our options
                 values.append(v) #add the value of that option
    if values:
        i = np.argmax(values) #find the max value
        x = options[i]      #set corresponding array to x and return

    return x



def fchc_transpose_neighbour(x):
    old_v = calc_value(x) #calculate current value of sack
    n = random.sample(range(0, 4), 2) #generates 2 diff numbers 0 - 4

    for i in range(5): #try to get a better value this many times
        r = x.copy() #copy our array
        if r[n[0]] != r[n[1]]: #if the 2 random items are not same
            r[n[0]], r[n[1]] = r[n[1]], r[n[0]] #swap the 2 random spots
            v = calc_value(r)
            w = calc_weight(r)
            if v > old_v and w <= 400:
                x = r
                break #break out of loop once find 1st better value

    return x

def hc_transpose_neighbour(x):
    old_v = calc_value(x) #calculate current value of sack
    n = list(itertools.combinations(range(5), 2)) #list of all possible 2 place swaps by index
    options = [] #store all viable options
    values = [] #store their values

    for tup in n: #for ea tuple of possible swap
        if x[tup[0]] != x[tup[1]]: #if the swap is not same value
            r = x.copy()
            r[tup[0]], r[tup[1]] = r[tup[1]], r[tup[0]] #make swap
            v = calc_value(r)
            w = calc_weight(r)
            if v > old_v and w <= 400:
                 options.append(r) #add potential choices to our options
                 values.append(v) #add the value of that option
    if values:
        i = np.argmax(values) #find the max value
        print("neighbours: " + str(options)) #*
        x = options[i]      #set corresponding array to x and return

    return x

def hc_large_neighbourhood():
    combinations = ["".join(seq) for seq in itertools.product("01", repeat=5)]
    values = []
    for c in combinations:
        i = list(c) #make a list of the diff combinations
        a = np.asarray(i) #make list to array
        a = [ int(x) for x in a ] #makes each item of array into int
        #print(a)
        w = calc_weight(a) #gets weight
        if w < 400:
            val = calc_value(a)
            #print(val)
        values.append(val)

    index, value = max(enumerate(values), key=operator.itemgetter(1))
    best = list(combinations[index])

    return best

def iterate_fchc_swap():
    a = 0
    change=True
    bag = random_start() #start with some random choices
    print("starting bag: " + str(bag))
    values=[]

    while change:
        bag = fchc_swap_neighbour(bag)
        a+= 1
        print(a)
        print(bag)
        v = calc_value(bag)
        values.append(v)
        try:
            if values[-2] == values[-1]:
                change = False
        except IndexError:
            pass


    return bag

def iterate_fchc_trans():
    a = 0
    change=True
    bag = random_start() #start with some random choices
    print("starting bag: " + str(bag))
    values=[]

    while change:
        for i in range(10):
            bag = fchc_transpose_neighbour(bag)
            a+= 1
            print(a)
            print(bag)
            v = calc_value(bag)
            values.append(v)


    return bag



'''ABORT**def iterate():
    change = True
    bag = random_start() #start with some random choices
    print(bag)
    local_bests = [] #returned from our hill climbing
    values = [] #values of the local_bests
    bests = []

    while change:
        for i in range(5): #do hc this many times
            new = hc_swap_neighbour(bag)
            local_bests.append(new)
            values.append(calc_value(new)) #add values of local_bests

        best_local = max(enumerate(values))
        bests.append(best_local)
        print(bests)
        same = 0
        best = np.argmax(bests)
        if len(bests) > 4:#if there are sufficiently many bests chosen from locals
            for item in bests:
                if item == best: #and if enough are the same
                    same += 1

        if same >= 9: #stop the algorithm
            change = False

    return best'''


if __name__ == '__main__':
    print(iterate())
    print(hc_large_neighbourhood())
    print(" ")

    bag = random_start()
    w1 = calc_weight(bag)
    v1 = calc_value(bag)
    print(bag)
    print('value ' + str(v1))
    print('weight ' + str(w1))
    '''new_bag = fchc_swap_neighbour(bag)
    print(new_bag)
    v2 = calc_value(new_bag)
    w2 = calc_weight(new_bag)
    print("value " + str(v2) + " weight " + str(w2))

    n2_bag = hc_swap_neighbour(bag)
    print(n2_bag)
    v3 = calc_value(n2_bag)
    w3 = calc_weight(n2_bag)
    print("hc swap value " + str(v3) + " weight " + str(w3))'''

    n3_bag = hc_transpose_neighbour(bag)
    print(n3_bag)
    v3 = calc_value(n3_bag)
    w3 = calc_weight(n3_bag)
    print("value " + str(v3) + " weight " + str(w3))
